set -x

sudo add-apt-repository ppa:neovim-ppa/stable
sudo apt update

sudo apt install git
sudo apt install git-gui
sudo apt install kitty
sudo apt install curl
sudo apt install neovim
sudo apt install neovim-qt
sudo apt install python3-pip






if [ ! -f ~/bin ];
then
    ln -sf ~/rc/bin ~/bin
fi

if [ ! -f ~/.config/nvim ];
then
    ln -sf ~/rc/nvim ~/.config/
fi

if [ ! -f ~/.config/kitty ];
then
    ln -sf ~/rc/kitty ~/.config/
fi


# Install font
mkdir -p ~/.fonts ; wget -qO- https://github.com/source-foundry/Hack/releases/download/v3.003/Hack-v3.003-ttf.tar.gz | tar xvz  -C ~/.fonts


pip3 install pynvim

curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.1/install.sh | bash

source ~/.nvm/nvm.sh

nvm install node

# setup plug
curl -fLo ~/.local/share/nvim/site/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim


~/rc/scripts/nvim_setup.py

echo "Setup Complete..."
echo "Use nx to start neovim"

echo "You may need to setup a compile_command.json to get completion to work"
