bval=$(xrandr --verbose | grep -m 1 -i brightness | cut -f2 -d ' ')
inc=0.1


mval="0.50"
if [ "$bval" = "$mval" ];
then
    echo "At $mval, not changing"
else
    bval="$(echo $bval - $inc | bc -l)"
    xrandr --output `xrandr | grep " connected" | cut -f1 -d " " ` --brightness $bval
fi
