"set guifont=Ubuntu\ Mono:h14
"set guifont=IBM\ Plex\ Mono\ Medium:h10
"set guifont=Hack:h11
"set guifont=JetBrains\ Mono:h16
call rpcnotify(0, 'Gui', 'WindowMaximized', 1)
"set guioptions+=a

"autocmd BufEnter,FocusGained,BufEnter,FocusLost,WinLeave * checktime

"colorscheme desert256
"colorscheme patine
"colorscheme LightYellow
"colorscheme 0x7A69_dark
"colorscheme 1989
"colorscheme mayansmoke
colorscheme bluegreen

"
" Enable Mouse
set mouse=a

GuiTabline 1


" Right Click Context Menu (Copy-Cut-Paste)
nnoremap <silent><RightMouse> :call GuiShowContextMenu()<CR>
inoremap <silent><RightMouse> <Esc>:call GuiShowContextMenu()<CR>
vnoremap <silent><RightMouse> :call GuiShowContextMenu()<CR>gv

syntax on

hi! Pmenu        ctermfg=0    ctermbg=7 guibg=7
hi! PmenuSel     ctermfg=7    ctermbg=0 guibg=0
hi! CocPumMenu   ctermfg=0    ctermbg=7 guibg=7
hi! CocMenuSel   ctermfg=7    ctermbg=0 guibg=red
hi! CocPumSearch ctermfg=NONE ctermbg=NONE cterm=underline


autocmd WinEnter,VimEnter,BufEnter * hi! CocMenuSel   ctermfg=7    ctermbg=0 guibg=red
