"Install vim plug using the command below
"curl -fLo ~/.local/share/nvim/site/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim


" Specify a directory for plugins
" " - For Neovim: ~/.local/share/nvim/plugged
" " - Avoid using standard Vim directory names like 'plugin'
" call plug#begin('~/.vim/plugged')
call plug#begin('~/.local/share/nvim/plugged')

Plug 'ntpeters/vim-better-whitespace'
Plug 'neoclide/coc.nvim', {'branch': 'release'}

Plug 'lpinilla/vim-codepainter'



" Auto close parens, braces, brackets, etc
" Plug 'jiangmiao/auto-pairs'


Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
Plug 'junegunn/fzf.vim'


"Plug 'Canop/patine'
"Plug 'jaxbot/semantic-highlight.vim'

"Plug 'roman/golden-ratio'

Plug 'liuchengxu/vista.vim'


Plug 'rhysd/vim-clang-format'
Plug 'sbdchd/neoformat'

Plug 'google/yapf'

Plug 'nvim-lua/plenary.nvim'
Plug 'hoschi/yode-nvim'

Plug 'fatih/vim-go', { 'do': ':GoUpdateBinaries' }
Plug 'folke/flash.nvim'
Plug 'lfv89/vim-interestingwords'

"Plug 'ray-x/go.nvim'
"Plug 'ray-x/guihua.lua'

"Plug 'Iron-E/nvim-highlite'
" Plug 'rakr/vim-one'


"Plug 'ctrlpvim/ctrlp.vim'

"From: https://bluz71.github.io/2017/10/26/turbocharge-the-ctrlp-vim-plugin.html
"let g:ctrlp_user_command = 'fd --type f --color=never "" %s'
"let g:ctrlp_use_caching = 0
"noremap <C-p>       :Files<CR>



" Colours
"Plug 'NLKNguyen/papercolor-theme'
"Plug 'lifepillar/vim-solarized8'


" Plug 'vim-airline/vim-airline'
" Plug 'vim-airline/vim-airline-themes'
" 
Plug 'flazz/vim-colorschemes'
Plug 'sainnhe/archived-colors'

" Plug 'scrooloose/nerdtree'
" Plug 'nathanaelkane/vim-indent-guides'

"Plug 'Yggdroot/indentLine'
"Plug 'vim-scripts/LargeFile'

" Plug 'python-mode/python-mode', { 'for': 'python', 'branch': 'develop' }
Plug 'akinsho/toggleterm.nvim', {'tag' : '*'}


" Initialize plugin system
call plug#end()


lua require("toggleterm").setup({ open_mapping = [[<a-t>]], start_in_insert = true,})

let mapleader = ","

cmap Wq wq


map <C-p>       :Files<CR>
map <C->      :Windows<CR>

tnoremap <Esc> <C-\><C-n>

tnoremap <C-p>       <C-\><C-n>:Files<CR>
tnoremap <C-;>       <C-\><C-n>:Windows<CR>

tnoremap <M-1> <C-\><C-n>:tabnext 1<CR>
tnoremap <M-2> <C-\><C-n>:tabnext 2<CR>
tnoremap <M-3> <C-\><C-n>:tabnext 3<CR>
tnoremap <M-4> <C-\><C-n>:tabnext 4<CR>
tnoremap <M-5> <C-\><C-n>:tabnext 5<CR>
tnoremap <M-6> <C-\><C-n>:tabnext 6<CR>
tnoremap <M-7> <C-\><C-n>:tabnext 7<CR>
tnoremap <M-8> <C-\><C-n>:tabnext 8<CR>
tnoremap <M-9> <C-\><C-n>:tabnext 9<CR>


" These are my extensions
if exists("$STING_ROOT")
    source $STING_ROOT/etc/dotvimrc
endif

filetype plugin indent on

"set cursorline
set tabstop=4
set shiftwidth=4
set expandtab
set hlsearch
set incsearch
set ignorecase
set smartcase
set wildmode=longest,list,full
set wildmenu
set autowrite

set autoindent
set ruler
set number
set splitright
set splitbelow
set nowrap
set hidden
"set nohidden
"set mouse=a
set wildignore+=*/tmp/*,*.so,*.swp,*.zip,*.cache,*.clangd     " Linux/MacOSX
"

"set clipboard+=unnamedplus
"
set clipboard+=unnamed
"set backspace=2
set backspace=indent,eol,start " backspace over everything in insert mode



" Interpret SConstruct files python files
au BufNewFile,BufRead *.xsh set filetype=python
"au BufReadPost *.xsh set syntax=python


"imap <S-Insert> "*pa

"nnoremap <S-Insert> "*p
"
"nnoremap <C-Insert> "+p<cr>
"nnoremap <C-Insert> "+p<cr>
imap <S-Insert> <MiddleMouse>
map! <S-Insert> <MiddleMouse>
cmap <S-Insert> <MiddleMouse>




"Search for tag file..here and above!
set tags=./tags;

" open the tags
"map <C-]> :tab split<CR>:exec("tselect ".expand("<cword>"))<CR>
map <C-]> :exec("tselect ".expand("<cword>"))<CR>
map <C-\> :vsp <CR>:exec("tselect ".expand("<cword>"))<CR>

"set textwidth=80 wrap
set colorcolumn=100
set formatoptions=tcqnr
set nospell




" Setting for asciidoc files
autocmd BufRead,BufNewFile *.txt,*.asciidoc,*.adoc,README,TODO,CHANGELOG,NOTES,ABOUT
        \ setlocal autoindent expandtab tabstop=8 softtabstop=2 shiftwidth=2 filetype=asciidoc spell
        \ textwidth=80 wrap formatoptions=tcqnrbl
        \ formatlistpat=^\\s*\\d\\+\\.\\s\\+\\\\|^\\s*<\\d\\+>\\s\\+\\\\|^\\s*[a-zA-Z.]\\.\\s\\+\\\\|^\\s*[ivxIVX]\\+\\.\\s\\+
        \ comments=s1:/*,ex:*/,://,b:#,:%,:XCOMM,fb:-,fb:*,fb:+,fb:.,fb:>



" Make sure that cursor is visible when we do an escape in terminal
" http://vimcasts.org/episodes/neovim-terminal-paste/
"hi! TermCursorNC ctermfg=15 guifg=#fdf6e3 ctermbg=14 guibg=#93a1a1 cterm=NONE gui=NONE

set title

autocmd! FileType fzf
autocmd  FileType fzf set laststatus=0 noshowmode noruler
  \| autocmd BufLeave <buffer> set laststatus=2 showmode ruler


syntax on



nnoremap <M-Down>  <C-W><C-J>
nnoremap <M-Up>    <C-W><C-K>
nnoremap <M-Right> <C-W><C-L>
nnoremap <M-Left>  <C-W><C-H>
nnoremap <M-k>  :tabnext
nnoremap <M-j>  :tabprev

nnoremap <M-n>  :bn
nnoremap <M-p>  :bp

inoremap <C-PageUp> :tabpreva
inoremap <C-PageDown> :tabnexta


"autocmd VimEnter * NERDTree

" Return to last edit position when opening files (You want this!)
autocmd BufReadPost *
     \ if line("'\"") > 0 && line("'\"") <= line("$") |
     \   exe "normal! g`\"" |
     \ endif


nmap <silent> gd <Plug>(coc-definition)
nmap <silent> gy <Plug>(coc-type-definition)
nmap <silent> gi <Plug>(coc-implementation)
nmap <silent> gr <Plug>(coc-references)

" Remap for rename current word
nmap <F2> <Plug>(coc-rename)

autocmd BufNew,BufEnter *.cpp,*.h execute "silent! CocEnable"
autocmd BufLeave *.cpp,*.h execute "silent! CocDisable"


au VimEnter *.conf execute "silent! CocDisable"

set signcolumn=no

"let g:airline_theme='cool'
"let g:airline_theme='one'
"let g:airline_theme='kolor'

"colorscheme plasticine
"colorscheme zellner


augroup ReduceNoise autocmd!
" " Automatically resize active split to 85 width
" autocmd WinEnter * :call ResizeSplits()
" augroup END
" 
" function! ResizeSplits()
"     set winwidth=162
"     wincmd =
" endfunction

nmap <F12> :call coc#float#close_all() <CR>



let g:pymode_lint_ignore = ['W0611', "E221", "E303", "E203"]


nmap <M-1> :tabnext 1
nmap <M-2> :tabnext 2
nmap <M-3> :tabnext 3
nmap <M-4> :tabnext 4
nmap <M-5> :tabnext 5
nmap <M-6> :tabnext 6
nmap <M-7> :tabnext 7
nmap <M-8> :tabnext 8
nmap <M-9> :tabnext 9


if !exists('g:lasttab')
  let g:lasttab = 1
endif
nmap ;;    :exe "tabn ".g:lasttab<CR>
nmap <M-;> :exe "tabn ".g:lasttab<CR>

"nmap <Leader>tl :exe "tabn ".g:lasttab<CR>
au TabLeave * let g:lasttab = tabpagenr()

if executable("ag")
    let $FZF_DEFAULT_COMMAND = 'ag -U -p .ignore --ignore .cache --ignore .clangd -g ""'
    
endif


" Disable coc for anything other than: 
let g:my_coc_file_types = ['c', 'cpp', 'h', 'hpp', 'vim', 'sh',  'go', 'rust']


function! s:disable_coc_for_type()
	if index(g:my_coc_file_types, &filetype) == -1
	        let b:coc_enabled = 0
	endif
endfunction

augroup CocGroup
	autocmd!
	autocmd BufNew,BufEnter * call s:disable_coc_for_type()

    "autocmd BufNew,BufEnter * hi! CocPumSearch ctermfg=NONE ctermbg=NONE cterm=underline
augroup end


" Add `:Format` command to format current buffer.
" command! -nargs=0 Format :call CocAction('format')
" 
" " Formatting selected code.
" xmap =  <Plug>(coc-format-selected)
" nmap =  <Plug>(coc-format-selected)


"vmap =  :Neoformat<CR>
"xmap =  :
"set equalprg=clang-format

"vmap =  :Neoformat<CR>

"g:clang_format#detect_style_file = 1

"vmap =  :ClangFormat<CR>
"autocmd BufNew,BufEnter *.cpp,*.h vmap :ClangFormat<CR>

"autocmd FileType cpp vmap <buffer> = :ClangFormat<CR>
autocmd FileType cpp vmap <buffer> = :ClangFormat<CR>

autocmd FileType python setlocal equalprg=vpf

lua require('yode-nvim').setup({})
map <Leader>yc      :YodeCreateSeditorFloating<CR>
map <Leader>yr :YodeCreateSeditorReplace<CR>
nmap <Leader>bd :YodeBufferDelete<cr>
imap <Leader>bd <esc>:YodeBufferDelete<cr>
" these commands fall back to overwritten keys when cursor is in split window
map <C-W>r :YodeLayoutShiftWinDown<CR>
map <C-W>R :YodeLayoutShiftWinUp<CR>
map <C-W>J :YodeLayoutShiftWinBottom<CR>
map <C-W>K :YodeLayoutShiftWinTop<CR>
" at the moment this is needed to have no gap for floating windows
set showtabline=2


"function! PFormat(start, end)
"    execute "0,$!yapf -l ".a:start."-".a:end
"endfunction


"autocmd FileType python vmap <buffer> = :call PFormat(line("'<"), line("'>"))
"autocmd FileType python vmap <buffer> =  :execute "0,$!yapf -l ".line("'<")."-".line("'>")

"vmap <buffer> = :call PFormat(line("'<"), line("'>"))
"autocmd FileType python vmap <buffer> = 0,$!yapf -l line("'<")-line("'>"))


inoremap <silent><expr> <CR> coc#pum#visible() ? coc#pum#confirm()
                              \: "\<C-g>u\<CR>\<c-r>=coc#on_enter()\<CR>"

g:interestingWordsCaseSensitive = 0


let g:better_whitespace_guicolor='#47D6FA'  




hi! Pmenu        ctermfg=0    ctermbg=7 guibg=7
hi! PmenuSel     ctermfg=7    ctermbg=0 guibg=0
hi! CocPumMenu   ctermfg=0    ctermbg=7 guibg=7
hi! CocMenuSel   ctermfg=7    ctermbg=0 guibg=red
hi! CocPumSearch ctermfg=NONE ctermbg=NONE cterm=underline




