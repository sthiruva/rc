
###### Setup all PATHs to lead our way....
#echo "Welcome $USER, bash is being setup"
export PS1='[\u@\h]\e[0;32m$(__git_ps1)\e[m \w
#->'

alias ls='ls --color'
alias ll='ls -l'

export PATH=$PATH:~/bin

function grepri()
{
    grep -Ri $1 * | grep -v ".svn"
}

function workon()
{
    if [ -z $1 ] || [ $1 == "." ];
    then
        work_dir=`pwd`
    else
        work_dir=$1
    fi

    export WORKDIR=$work_dir
    unset work_dir

    echo "Working on: $WORKDIR"
}

#cd to the work dir
function w()
{
    if [ -z $WORKDIR ];
    then
        echo "WORKDIR not set. Please run the workon command before you user w()"
    else
        cd $WORKDIR
    fi
}


export EDITOR=vim


alias mpg123='mpg123 -C'
#alias gvim='/usr/bin/gvim -p'

# alias gvim='nvim-qt -- -O'
# alias gvimdiff='nvim-qt -- -d'


# Avoid duplicates
export HISTCONTROL=ignoredups:erasedups
# When the shell exits, append to the history file instead of overwriting it
shopt -s histappend
shopt -s direxpand

# After each command, append to the history file and reread it
#export PROMPT_COMMAND="${PROMPT_COMMAND:+$PROMPT_COMMAND$'\n'}history -a; history -c; history -r"

# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTSIZE=100000
HISTFILESIZE=200000


#alias ting='paplay /usr/share/sounds/freedesktop/stereo/complete.oga'


function old_s()
{
    cd ~/work/sting/
    source dotbashrc

    if [ ! -z $TMUX ];
    then

        cd ~/work/sting; source dotbashrc

        return
    fi



    sname="sting"
    tmux has-session -t $sname &> /dev/null

    if [ $? != 0 ];
    then
        tmux new-session -s $sname -d
        tmux new-window
        tmux new-window
        tmux send-keys -t $sname:0 "cd ~/work/sting; source dotbashrc" C-m
        tmux send-keys -t $sname:1 "cd ~/work/sting; source dotbashrc" C-m
        tmux send-keys -t $sname:2 "cd ~/work/sting; source dotbashrc" C-m
        tmux next-window -t $sname
    fi

    tmux a -t $sname
}

alias hs='tmux split-window -v'
alias vs='tmux split-window -h'
alias nt='tmux new-window'


#determines search program for fzf
if type ag &> /dev/null; then
    export FZF_DEFAULT_COMMAND='ag -p .gitignore --ignore .cache --ignore .clangd -g ""'
    #export FZF_DEFAULT_OPTS=" --inline-info --border --color 'fg:#bbccdd,fg+:#ddeeff,bg:#334455,border:#778899'"
    export FZF_DEFAULT_OPTS=" --inline-info --border --color 'fg:#173f5f,fg+:#494949,bg:#f4f1bc,border:#ed553b,bg+:#c5e2ee,hl+:#1b00ff,hl:#1b00ff'"
fi

## Solarized colors
#export FZF_DEFAULT_OPTS='
#  --color=bg+:#073642,bg:#002b36,spinner:#719e07,hl:#586e75
#  --color=fg:#839496,header:#586e75,info:#cb4b16,pointer:#719e07
#  --color=marker:#719e07,fg+:#839496,prompt:#719e07,hl+:#719e07
#'


export FZF_DEFAULT_OPTS=$FZF_DEFAULT_OPTS'
--color=dark
--color=fg:-1,bg:-1,hl:#5fff87,fg+:-1,bg+:-1,hl+:#ffaf5f
--color=info:#af87ff,prompt:#5fff87,pointer:#ff87d7,marker:#ff87d7,spinner:#ff87d7
'

eval "$RUN"


alias xt="tabbed -c xterm -T $HOSTNAME -maximized -fn 9x15 -into"

# export NVR_CMD=nvim-qt
# #export NVIM_LISTEN_ADDRESS=/tmp/nvimsocket
# function nv()
# {
#     (nvr -s --nostart --remote-tab "$@" && (wmctrl -lx | grep NVIM | cut -f 1 -d " " |  xargs wmctrl -i -a) )|| nvim-qt "$@"
# }
# 
# 
# 
# function nt()
# {
#     (nvr -s --nostart --remote-tab "$@" && (wmctrl -lx | grep NVIM | cut -f 1 -d " " |  xargs wmctrl -i -a) )|| nvim-qt "$@"
# }

#alias gvim=nx
#alias g=/usr/bin/gvim







#eval "$(fasd --init auto)" 
#alias g="f -e gvim"
#
#_fasd_bash_hook_cmd_complete  g  m  j  o
#m j o

export PATH=$PATH:~/bin/

wmctrl -r "Workrave" -b add,skip_taskbar

alias x='xmodmap -e "keycode 119 = Insert Insert Insert Insert"'

