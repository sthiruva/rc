#!/usr/bin/env python3

import os
import sys
import neovim
import json
import time



sock_file="/tmp/nvim_setup_soc_file"
nla = "NVIM_LISTEN_ADDRESS=%s" % (sock_file)
cmd = "%s nvim-qt " % (nla)
os.system(cmd)

time.sleep(2)

nvim = neovim.attach("socket", path=sock_file)
nvim.command('PlugInstall')
time.sleep(2)
nvim.command('CocInstall coc-clangd')
time.sleep(2)
nvim.command('set filetype=cpp')
nvim.command('CocCommand clangd.install')

time.sleep(2)

nvim.quit()

