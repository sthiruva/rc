" Install plug with command in a terminal: 
" curl -fLo ~/.vim/autoload/plug.vim --create-dirs  https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
call plug#begin('~/.vim/plugged')



Plug 'ntpeters/vim-better-whitespace'
Plug 'neoclide/coc.nvim', {'branch': 'release'}


" Auto close parens, braces, brackets, etc
" Plug 'jiangmiao/auto-pairs'


Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
Plug 'junegunn/fzf.vim'

Plug 'Canop/patine'
Plug 'jaxbot/semantic-highlight.vim'

"Plug 'roman/golden-ratio'

Plug 'liuchengxu/vista.vim'

Plug 'Iron-E/nvim-highlite'
Plug 'rakr/vim-one'


"Plug 'ctrlpvim/ctrlp.vim'

"From: https://bluz71.github.io/2017/10/26/turbocharge-the-ctrlp-vim-plugin.html
"let g:ctrlp_user_command = 'fd --type f --color=never "" %s'
"let g:ctrlp_use_caching = 0
"noremap <C-p>       :Files<CR>
"
noremap <A-p>       :Buffers<CR>


" Colours
Plug 'NLKNguyen/papercolor-theme'
Plug 'lifepillar/vim-solarized8'


Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'

Plug 'flazz/vim-colorschemes'
" Plug 'scrooloose/nerdtree'
" Plug 'junegunn/limelight.vim'
" Plug 'nathanaelkane/vim-indent-guides'

Plug 'Yggdroot/indentLine'

" Plug 'python-mode/python-mode', { 'for': 'python', 'branch': 'develop' }


" Initialize plugin system
call plug#end()


let mapleader = ","

cmap Wq wq


map <C-p>       :Files<CR>
map <C-;>       :Buffers<CR>


" These are my extensions
if exists("$STING_ROOT")
    source $STING_ROOT/etc/dotvimrc
endif

filetype plugin indent on

"set cursorline
set tabstop=4
set shiftwidth=4
set expandtab
set hlsearch
set incsearch
set ignorecase
set smartcase
set wildmode=longest,list,full
set wildmenu
set autowrite

set autoindent
set ruler
set number
set splitright
set splitbelow
set nowrap
set hidden
"set mouse=a
set wildignore+=*/tmp/*,*.so,*.swp,*.zip,*.cache,*.clangd     " Linux/MacOSX
"

"set clipboard=unnamed
"set backspace=2
set backspace=indent,eol,start " backspace over everything in insert mode



" Interpret SConstruct files python files
au BufNewFile,BufRead SCons* set filetype=python







"Search for tag file..here and above!
set tags=./tags;

" open the tags
"map <C-]> :tab split<CR>:exec("tselect ".expand("<cword>"))<CR>
map <C-]> :exec("tselect ".expand("<cword>"))<CR>
map <C-\> :vsp <CR>:exec("tselect ".expand("<cword>"))<CR>

"set textwidth=80 wrap
set colorcolumn=80
set formatoptions=tcqnr
set nospell




" Setting for asciidoc files
autocmd BufRead,BufNewFile *.txt,*.asciidoc,*.adoc,README,TODO,CHANGELOG,NOTES,ABOUT
        \ setlocal autoindent expandtab tabstop=8 softtabstop=2 shiftwidth=2 filetype=asciidoc spell
        \ textwidth=80 wrap formatoptions=tcqnrbl
        \ formatlistpat=^\\s*\\d\\+\\.\\s\\+\\\\|^\\s*<\\d\\+>\\s\\+\\\\|^\\s*[a-zA-Z.]\\.\\s\\+\\\\|^\\s*[ivxIVX]\\+\\.\\s\\+
        \ comments=s1:/*,ex:*/,://,b:#,:%,:XCOMM,fb:-,fb:*,fb:+,fb:.,fb:>



" Make sure that cursor is visible when we do an escape in terminal
" http://vimcasts.org/episodes/neovim-terminal-paste/
hi! TermCursorNC ctermfg=15 guifg=#fdf6e3 ctermbg=14 guibg=#93a1a1 cterm=NONE gui=NONE

set title

autocmd! FileType fzf
autocmd  FileType fzf set laststatus=0 noshowmode noruler
  \| autocmd BufLeave <buffer> set laststatus=2 showmode ruler


syntax on



nnoremap <M-Down>  <C-W><C-J>
nnoremap <M-Up>    <C-W><C-K>
nnoremap <M-Right> <C-W><C-L>
nnoremap <M-Left>  <C-W><C-H>
nnoremap<M-k>  :tabNext
nnoremap<M-j>  :tabprev

nnoremap <M-n>  :bn
nnoremap <M-p>  :bp


:tnoremap <Esc> <C-\><C-n>

"autocmd VimEnter * NERDTree

" Return to last edit position when opening files (You want this!)
autocmd BufReadPost *
     \ if line("'\"") > 0 && line("'\"") <= line("$") |
     \   exe "normal! g`\"" |
     \ endif


nmap <silent> gd <Plug>(coc-definition)
nmap <silent> gy <Plug>(coc-type-definition)
nmap <silent> gi <Plug>(coc-implementation)
nmap <silent> gr <Plug>(coc-references)

" Remap for rename current word
nmap <F2> <Plug>(coc-rename)

autocmd BufNew,BufEnter *.cpp,*.h,*.py execute "silent! CocEnable"
autocmd BufLeave *.cpp,*.h,*.py execute "silent! CocDisable"


au VimEnter *.conf execute "silent! CocDisable"

set signcolumn=yes

"let g:airline_theme='cool'
"let g:airline_theme='one'
let g:airline_theme='kolor'


"colorscheme highlite
"colorscheme material-theme
"colorscheme space-vim-dark
"colorscheme one
"colorscheme Light
"colorscheme softlight
colorscheme mayansmoke
"colorscheme patine


augroup ReduceNoise autocmd!
" Automatically resize active split to 85 width
autocmd WinEnter * :call ResizeSplits()
augroup END

function! ResizeSplits()
    set winwidth=85
    wincmd =
endfunction

nmap <F12> :call coc#float#close_all() <CR>



let g:pymode_lint_ignore = ['W0611', "E221", "E303", "E203"]


nmap <M-1> :tabnext 1
nmap <M-1> :tabnext 2
nmap <M-1> :tabnext 3
nmap <M-1> :tabnext 4
nmap <M-1> :tabnext 5
nmap <M-1> :tabnext 6
nmap <M-1> :tabnext 7
nmap <M-1> :tabnext 8
nmap <M-1> :tabnext 8

if executable("ag")
    let $FZF_DEFAULT_COMMAND = 'ag -U --hidden --ignore .git --ignore tools/ -g ""'
endif
